/* Info :
 Black Spider Level: 60 - 79 
 Player :  5 - 15 :
 Delay : 120 min. 
 Reputation : +1800 
 Gold : 7000 - 8000
 Data : 
 Damage% : no
 TeleportAll : no
 OneSeal : yes
 */
DECLARE @BoxId SMALLINT = 302
DECLARE @BoxDesign SMALLINT = 3


INSERT INTO [dbo].[RollGeneratedItem] 
(
	[OriginalItemDesign], [OriginalItemVNum],
	[MinimumOriginalItemRare], [MaximumOriginalItemRare], [ItemGeneratedVNum],
	[ItemGeneratedDesign],	[ItemGeneratedAmount], [IsRareRandom],
	[Probability]
)
VALUES

	(@BoxDesign, @BoxId, '0', '7', '316', '0', '1', '0', '2'),/* Gant de la tempête */
	(@BoxDesign, @BoxId, '0', '7', '319', '0', '1', '0', '2'),/* chaussures du feu */
	(@BoxDesign, @BoxId, '0', '7', '141', '0', '1', '0', '2'),/* épée assassine */
	(@BoxDesign, @BoxId, '0', '7', '148', '0', '1', '0', '2'),/* Arc se siège */
	(@BoxDesign, @BoxId, '0', '7', '155', '0', '1', '0', '2'),/* Baguette magique de l'âme */
	(@BoxDesign, @BoxId, '0', '7', '292', '0', '1', '0', '2'),/* Arbalète Balenty */
	(@BoxDesign, @BoxId, '0', '7', '290', '0', '1', '0', '2'),/* Kriss */
	(@BoxDesign, @BoxId, '0', '7', '294', '0', '1', '0', '2'),/* Arme enchantée de la lumière */
	(@BoxDesign, @BoxId, '0', '7', '298', '0', '1', '0', '2'), /* Défenseur splendide */
	(@BoxDesign, @BoxId, '0', '7', '296', '0', '1', '0', '2'), /* Robe de lumière */
	(@BoxDesign, @BoxId, '0', '7', '272', '0', '1', '0', '2'), /* Robe D'essai */
	(@BoxDesign, @BoxId, '0', '7', '9360', '0', '1', '0', '1') /* Poltron */
